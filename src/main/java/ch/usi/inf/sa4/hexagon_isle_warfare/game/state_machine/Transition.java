package ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine;

/**
 * 
 * @author Simone Raimondi
 *	Enum class to store the transitions of the state machine class
 */
public enum Transition {
	HQ_CLICK, 
	MINE_CLICK,
	UNIT_CLICK,
	TARGET_CLICK,
	ACTION_CLICK,
	ENDED_ACTION,
	FAILED_ACTION,
	EXIT_POPUP
}

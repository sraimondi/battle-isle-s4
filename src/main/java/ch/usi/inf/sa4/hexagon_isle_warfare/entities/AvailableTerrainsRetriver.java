package ch.usi.inf.sa4.hexagon_isle_warfare.entities;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.map.GroundTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.ConcreteTypes;;

public class AvailableTerrainsRetriver {
	public static List<GroundTypes> availableTerrain(final ConcreteTypes unitType) {
		final List<GroundTypes> availableTerrains = new ArrayList<>();
		
		switch (unitType) {
		case TEST_UNIT:
			availableTerrains.add(GroundTypes.PLAIN);
			availableTerrains.add(GroundTypes.SAND);
			availableTerrains.add(GroundTypes.HILL);
			break;

		default:
			break;
		}
		return availableTerrains;
	}
}

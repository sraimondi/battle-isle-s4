package ch.usi.inf.sa4.hexagon_isle_warfare.utilities.quad_tree.bounds;


import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.quad_tree.QuadTreeElement;

/**
 * @author Simone Raimondi
 * Created on 03/03/14.
 */
public class QuadRect {
    private final Vector2D topLeftCorner;
    private final int height;
    private final int width;

    /**
     * Constructor for rectangular bounds of the quad tree
     * @param topLeft Top left corner of the bounds
     * @param height Height of the bounds
     * @param width Width of the bounds
     */
    public QuadRect(final Vector2D topLeft, final int height, final int width) {
        topLeftCorner = topLeft;
        this.width = width;
        this.height = height;
    }

    /**
     * Getter for the top left corner
     * @return The top left corner of the bounds
     */
    public Vector2D getTopLeftCorner() {
        return topLeftCorner;
    }

    /**
     * Getter for the height of the bounds
     * @return Height of the bounds
     */
    public int getHeight() {
      return height;
    }

    /**
     * Getter for the height of the bounds
     * @return Width of the bounds
     */
    public int getWidth() {
        return width;
    }

    /**
     * Method to check if the object is inside the bounds
     * @param object The object to check
     * @return Calls to the object method isInside
     */
    public <E extends QuadTreeElement> boolean contains(E object) {
        return object.isInside(this);
    }
}

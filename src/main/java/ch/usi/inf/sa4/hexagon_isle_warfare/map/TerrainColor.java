package ch.usi.inf.sa4.hexagon_isle_warfare.map;

import java.awt.Color;

public class TerrainColor {
	
	/**
	 * Set color for deep water terrain. (dark blue)
	 */
	private static final Color deepWaterColor = new Color(0, 0, 153);
	
	/**
	 * Set color for medium water terrain. (blue)
	 */
	private static final Color mediumWaterColor = new Color(0, 0, 255);
	
	/**
	 * Set color for shallow water terrain. (light blue)
	 */
	private static final Color shallowWaterColor = new Color(0, 128, 255);
	
	/**
	 * Set color for sand terrain. (yellow)
	 */
	private static final Color sandColor = new Color(255, 255, 153);
	
	/**
	 * Set color for road terrain. (dark grey)
	 */
	private static final Color roadColor = new Color(128, 128, 128);
	
	/**
	 * Set color for plains terrain. (light green)
	 */
	private static final Color plainsColor = new Color(100, 153, 0);
	
	/**
	 * Set color for hills terrain. (green)
	 */
	private static final Color hillsColor = new Color(153, 76, 0);
	
	/**
	 * Set color for mountain terrain. (green)
	 */
	private static final Color mountainColor = new Color(160, 160, 160);
	
	/**
	 * Set color for energycell terrain. (green)
	 */
	private static final Color energycellColor = new Color(204, 0, 204);
	
	
	/**
	 * Method for retrieving the color based on the terrain type.
	 * @param type The terrain type.
	 * @return returnColor The color of that type of terrain.
	 */
	public static Color retriveColor(final GroundTypes type) {
		Color returnColor;
		switch (type) {
			case DEEPWATER:
				returnColor = deepWaterColor;
				break;
			case MEDIUMWATER:
				returnColor = mediumWaterColor;
				break;
			case SHALLOWWATER:
				returnColor = shallowWaterColor;
				break;
			case SAND:
				returnColor = sandColor;
				break;
			case ROAD:
				returnColor = roadColor;
				break;
			case PLAIN:
				returnColor = plainsColor;
				break;
			case HILL:
				returnColor = hillsColor;
				break;
			case MOUNTAIN:
				returnColor = mountainColor;
				break;
			case ENERGYCELL:
				returnColor = energycellColor;
				break;
			
			default:
				System.out.println("No color found for terrain type given");
				returnColor = null;
				break;
		}
		return returnColor;
	}
}

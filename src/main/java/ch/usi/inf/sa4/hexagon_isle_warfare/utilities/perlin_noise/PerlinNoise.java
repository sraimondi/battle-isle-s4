package ch.usi.inf.sa4.hexagon_isle_warfare.utilities.perlin_noise;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.octave.IslandGradient;
import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.octave.Octave;

/**
 * @author Simone Raimondi Created on 11/03/14.
 */
public class PerlinNoise {

	private final int width;
	private final int height;
	private final double initNoise[][];
	private final double perlinNoise[][];
	private final List<double[][]> noises;

	/**
	 * Perlin noise constructor
	 * @param height Height in pixel of the noise
	 * @param width Width in pixel of the noise
	 * @param islandShape Boolean value to indicate if you want the noise to be generated with the form of an island
	 * @param groundPercent	Percentage of ground for the map
	 */
	public PerlinNoise(final int height, final int width, final boolean islandShape,
			final double groundPercent) {

		this.height = height;
		this.width = width;

		initNoise = initNoise(height - 2 * Constants.OFFSET_WATER, width - 2
				* Constants.OFFSET_WATER);

		noises = new ArrayList<>();

		for (int i = 4; i < 7; i++) {
			noises.add(new Octave(initNoise, i).getNoise());
		}

		if (islandShape) {
			IslandGradient grad = new IslandGradient(height, width, groundPercent);
			noises.add(grad.getNoise());
		}

		perlinNoise = generatePerlinNoise();

	}
	

	/**
	 * Generate a new Perlin noise
	 * @return A double array with all the values of the Perlin noise normalized
	 */
	private double[][] generatePerlinNoise() {
		double perlinNoise[][] = new double[height][width];

		double persistance = 0.5;
		double amplitude = 1.0;
		double totalAmplitude = 0.0;

		for (int row = 0; row < height; row++)
			for (int col = 0; col < width; col++) {
				perlinNoise[row][col] += noises.get(noises.size() - 1)[row][col]
						* amplitude;
			}
		amplitude *= persistance;
		totalAmplitude += amplitude;

		// Merge noises
		for (int noiseIndex = noises.size() - 2; noiseIndex >= 0; noiseIndex--) {
			amplitude *= persistance;
			totalAmplitude += amplitude;

			for (int row = Constants.OFFSET_WATER; row < height
					- Constants.OFFSET_WATER; row++)
				for (int col = Constants.OFFSET_WATER; col < width
						- Constants.OFFSET_WATER; col++) {
					perlinNoise[row][col] += noises.get(noiseIndex)[row
							- Constants.OFFSET_WATER][col
							- Constants.OFFSET_WATER]
							* amplitude;
				}
		}

		// Normalise
		for (int row = 0; row < height; row++)
			for (int col = 0; col < width; col++) {
				perlinNoise[row][col] /= totalAmplitude;
			}

		return perlinNoise;
	}

	/**
	 * Initialise the first noise to use as base noise
	 * @param height Height of the base noise
	 * @param width Width of the base noise
	 * @return	double array that contains the random initialized
	 */
	private double[][] initNoise(final int height, final int width) {
		double noise[][] = new double[height][width];
		for (int row = 0; row < height; row++)
			for (int col = 0; col < width; col++) {
				noise[row][col] = Math.random();
			}
		return noise;
	}

	/**
	 * Getter for the higher value in the Perlin noise array
	 * @return Maximum of the array
	 */
	public double getMax() {
		double max = perlinNoise[0][0];
		for (int row = 0; row < perlinNoise.length; row++)
			for (int col = 0; col < perlinNoise[0].length; col++) {
				if (perlinNoise[row][col] >= max)
					max = perlinNoise[row][col];
			}
		return max;
	}

	/**
	 * Getter for the smallest value in the Perlin noise array
	 * @return Minimum of the array
	 */
	public double getMin() {
		double min = perlinNoise[0][0];
		for (int row = 0; row < perlinNoise.length; row++)
			for (int col = 0; col < perlinNoise[0].length; col++) {
				if (perlinNoise[row][col] <= min)
					min = perlinNoise[row][col];
			}
		return min;
	}

	/**
	 * Getter for the generated noise in the array
	 * @return double array with the noise
	 */
	public double[][] getPerlinNoise() {
		return perlinNoise;
	}

}

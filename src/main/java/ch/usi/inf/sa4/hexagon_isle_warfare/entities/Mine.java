package ch.usi.inf.sa4.hexagon_isle_warfare.entities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public class Mine extends Building {
	
	private final Integer productionPerTurn;
	
	public Mine(final Integer health, 
			final Integer missChance, 
			final Player owner, 
			final Hexagon cell,  
			final String name, 
			final ConcreteTypes unitType){
		super(health, missChance, owner, cell, name, unitType);
		this.productionPerTurn = Constants.MINEPRODUCTIONENERGY;
	}

	public Integer getProductionPerTurn() {
		return productionPerTurn;
	}

	@Override
	public void draw(Graphics2D g) {
		Vector2D vertices[] = this.getCell().getVertices();
		int xPoly[] = new int[6];
		int yPoly[] = new int[6];
		// Center colored
		Polygon p = new Polygon();
		for (int vertex = 0; vertex < vertices.length; vertex++) {
			xPoly[vertex] = (int) vertices[vertex].getX();
			yPoly[vertex] = (int) vertices[vertex].getY();
			p.addPoint((int) vertices[vertex].getX(),
					(int) vertices[vertex].getY());
		}
		// Draw center
		g.setColor(Constants.MINECOLOR);
		g.fillPolygon(p);
		// Draw border
		g.setColor(Color.BLACK);
		g.drawPolygon(xPoly, yPoly, xPoly.length);
	}

	@Override
	public void setColors(Color... newColor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> retriveActions() {
		return null;
	}
}

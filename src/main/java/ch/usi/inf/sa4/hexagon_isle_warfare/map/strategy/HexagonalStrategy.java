package ch.usi.inf.sa4.hexagon_isle_warfare.map.strategy;

import java.util.HashMap;

import ch.usi.inf.sa4.hexagon_isle_warfare.event_listener.EventListener;
import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.graphic.Drawable;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.Map;
import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.ArrayCoordinate;

public class HexagonalStrategy<T extends Drawable & EventListener> implements BoardStrategy<T> {

	
	@Override
	public HashMap<Integer, T> getNeighbours(final Map<T> boardReference, final int row, final int col) throws IndexOutOfBoundsException {
		if (row < 0 || row >= boardReference.getHeight() || col < 0 || col >= boardReference.getWidth()) throw new IndexOutOfBoundsException();
        else {
            if (col % 2 == 0) {
                return computeNeighbourEvenCol(boardReference, row, col);
            } else {
                return computeNeighbourOddCol(boardReference, row, col);
            }
        }
	}
	
	@Override
	public HashMap<Integer, ArrayCoordinate> getNeighboursCoordinates(final Map<T> boardReference, final int row, final int col) throws IndexOutOfBoundsException {
		if (row < 0 || row >= boardReference.getHeight() || col < 0 || col >= boardReference.getWidth()) throw new IndexOutOfBoundsException();
		else {
			if (col % 2 == 0) {
				return computeNeighbourEvenColCoordinates(boardReference, row, col);
			} else {
				return computeNeighbourOddColCoordinates(boardReference, row, col);
			}
		}
	}
	
	@Override
	public HashMap<Integer, T> getNeighbour(Map<T> boardReference, T cell) throws InvalidInputException {
		final int mapWidth = boardReference.getWidth();
		final int mapHeight = boardReference.getHeight();
		
		for (int row = 0; row < mapHeight; row++)
			for (int col = 0; col < mapWidth; col++) {
				if (boardReference.getCellAt(row, col).equals(cell)) {
					return getNeighbours(boardReference, row, col);
				}
				
		}
		throw new InvalidInputException("Cell doesn't exist");
		
	}
	

	
	/**
     * Compute the neighbours for odd column
     * @param boardReference the board to use to compute the neighbours
     * @param row the row of the cell you want to get the neighbours
     * @param col the column of the cell you want to get the neighbours
     * @return an hashmap with all the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
     */
    private HashMap<Integer, T> computeNeighbourOddCol(final Map<T> boardReference, final int row, final int col) {
        HashMap<Integer, T> neighbours = new HashMap<>();
        final int colLength = boardReference.getHeight();
        final int rowLength = boardReference.getWidth();
        //Neighbour 0
        if (row - 1 >= 0) neighbours.put(0, boardReference.getCellAt(row - 1, col));
        //Neighbour 1
        if (col < rowLength - 1) neighbours.put(1, boardReference.getCellAt(row, col + 1));
        //Neighbour 2
        if (row < colLength - 1 && col < rowLength - 1) neighbours.put(2, boardReference.getCellAt(row + 1, col + 1));
        //Neighbour 3
        if (row < colLength - 1) neighbours.put(3, boardReference.getCellAt(row + 1, col));
        //Neighbour 4
        if (row < colLength - 1 && col - 1 >= 0) neighbours.put(4, boardReference.getCellAt(row + 1, col - 1));
        //Neighbour 5
        if (col - 1 >= 0) neighbours.put(5, boardReference.getCellAt(row, col - 1));

        return neighbours;
    }
    
    /**
     * Compute the neighbours for even column
     * @param boardReference the board to use to compute the neighbours
     * @param row the row of the cell you want to get the neighbours
     * @param col the column of the cell you want to get the neighbours
     * @return an hashmap with all the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
     */
    private HashMap<Integer, T> computeNeighbourEvenCol(final Map<T> boardReference, final int row, final int col) {
        HashMap<Integer, T> neighbours = new HashMap<>();
        final int colLength = boardReference.getHeight();
        final int rowLength = boardReference.getWidth();

        //Neighbour 0
        if (row - 1 >= 0) neighbours.put(0, boardReference.getCellAt(row - 1, col));
        //Neighbour 1
        if (row - 1 >= 0 && col < rowLength - 1) neighbours.put(1, boardReference.getCellAt(row - 1, col + 1));
        //Neighbour 2
        if (col < rowLength - 1) neighbours.put(2, boardReference.getCellAt(row, col + 1));
        //Neighbour 3
        if (row < colLength - 1) neighbours.put(3, boardReference.getCellAt(row + 1, col));
        //Neighbour 4
        if (col - 1 >= 0) neighbours.put(4, boardReference.getCellAt(row, col - 1));
        //Neighbour 5
        if (row - 1 >= 0 && col - 1 >= 0) neighbours.put(5, boardReference.getCellAt(row - 1, col - 1));

        return neighbours;
    }
    
    /**
     * Compute the neighbours coordinates for odd column
     * @param boardReference the board to use to compute the neighbours
     * @param row the row of the cell you want to get the neighbours
     * @param col the column of the cell you want to get the neighbours
     * @return an hashmap with all the coordinates of the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
     */
    private HashMap<Integer, ArrayCoordinate> computeNeighbourOddColCoordinates(final Map<T> boardReference, final int row, final int col) {
    	HashMap<Integer, ArrayCoordinate> neighbours = new HashMap<>();
    	final int colLength = boardReference.getHeight();
        final int rowLength = boardReference.getWidth();
        //Neighbour 0
        if (row - 1 >= 0) neighbours.put(0, new ArrayCoordinate(row - 1, col));
        //Neighbour 1
        if (col < colLength - 1) neighbours.put(1, new ArrayCoordinate(row, col + 1));
        //Neighbour 2
        if (row < rowLength - 1 && col < colLength - 1) neighbours.put(2, new ArrayCoordinate(row + 1, col + 1));
        //Neighbour 3
        if (row < rowLength - 1) neighbours.put(3, new ArrayCoordinate(row + 1, col));
        //Neighbour 4
        if (row < rowLength - 1 && col - 1 >= 0) neighbours.put(4, new ArrayCoordinate(row + 1, col - 1));
        //Neighbour 5
        if (col - 1 >= 0) neighbours.put(5, new ArrayCoordinate(row, col - 1));

        return neighbours;
    }
    
    /**
     * Compute the neighbours coordinates for even column
     * @param boardReference the board to use to compute the neighbours
     * @param row the row of the cell you want to get the neighbours
     * @param col the column of the cell you want to get the neighbours
     * @return an hashmap with all the coordinates of the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
     */
    private HashMap<Integer, ArrayCoordinate> computeNeighbourEvenColCoordinates(final Map<T> boardReference, final int row, final int col) {
    	HashMap<Integer, ArrayCoordinate> neighbours = new HashMap<>();
    	final int colLength = boardReference.getHeight();
        final int rowLength = boardReference.getWidth();

        //Neighbour 0
        if (row - 1 >= 0) neighbours.put(0, new ArrayCoordinate(row - 1, col));
        //Neighbour 1
        if (row - 1 >= 0 && col < colLength - 1) neighbours.put(1, new ArrayCoordinate(row - 1, col + 1));
        //Neighbour 2
        if (col < colLength - 1) neighbours.put(2, new ArrayCoordinate(row, col + 1));
        //Neighbour 3
        if (row < rowLength - 1) neighbours.put(3, new ArrayCoordinate(row + 1, col));
        //Neighbour 4
        if (col - 1 >= 0) neighbours.put(4, new ArrayCoordinate(row, col - 1));
        //Neighbour 5
        if (row - 1 >= 0 && col - 1 >= 0) neighbours.put(5, new ArrayCoordinate(row - 1, col - 1));

        return neighbours;
    }

	
}

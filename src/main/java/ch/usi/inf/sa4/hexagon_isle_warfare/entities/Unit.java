package ch.usi.inf.sa4.hexagon_isle_warfare.entities;

import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;
/**
 * 
 * @author Matteo Morisoli
 *
 */
public abstract class Unit extends Entity {
	
	protected Integer movementRange;
	protected Integer attackDamage;
	protected Integer attackRange;
	
	/**
	 * 
	 * @param health
	 * @param missChance
	 * @param owner
	 * @param cell
	 * @param movementRange
	 * @param attackDamage
	 * @param attackRange
	 * @param name
	 * @param unitType
	 */
	public Unit(final Integer health, 
				final Integer missChance, 
				final Player owner, 
				final Hexagon cell, 
				final Integer movementRange, 
				final Integer attackDamage, 
				final Integer attackRange,
				final String name,
				final ConcreteTypes unitType){
		super(health, missChance, owner, cell, name, unitType);
		this.movementRange = movementRange;
		this.attackDamage  = attackDamage;
		this.attackRange = attackRange;
	}

	public Integer getMovementRange() {
		return movementRange;
	}

	public void setMovementRange(Integer movementRange) {
		this.movementRange = movementRange;
	}
	
	public Integer getAttackDamage() {
		return attackDamage;
	}

	public void setAttackDamage(Integer attackDamage) {
		this.attackDamage = attackDamage;
	}
	
	public Integer getAttackRange() {
		return movementRange;
	}

	public void setAttackRange(Integer attackRange) {
		this.attackRange =  attackRange;
	}
}

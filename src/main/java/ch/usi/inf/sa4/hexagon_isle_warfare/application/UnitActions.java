package ch.usi.inf.sa4.hexagon_isle_warfare.application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import ch.usi.inf.sa4.hexagon_isle_warfare.action.MoveAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.SpawnMineAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Game;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.GameStateMachine;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.Transition;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public class UnitActions extends JDialog {
	
	public UnitActions(JFrame parent, String title, final Vector2D position, final GameStateMachine gsm, final Game game, final Unit targetUnit){
		super(parent, title, true);
        if (parent != null) {
            Dimension parentSize = parent.getSize();
            Point p = parent.getLocation();
            /**Position where the pop up appears*/
            setLocation((int)position.getX(), (int)position.getY());
        }

        JPanel actionPane = new JPanel();
        //JPanel internalMessagePane = new JPanel();

        //actionPane.setLayout(new BorderLayout());
        actionPane.setPreferredSize(new Dimension(200, 100));   //CHANGE DIMENSION TO THE MESSAGE
        
        
        //Add actions

        JButton move = new JButton("Move");
        actionPane.add(move);
        
        move.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				gsm.performEvent(Transition.ACTION_CLICK);
				
				final MoveAction move = new MoveAction(targetUnit.getOwner(), targetUnit, null);


				
				setVisible(false);
		        dispose();
			}
		});
        	
        
        
        
        
        //actionPane.add(text, BorderLayout.CENTER);  //JTextArea is for editable text. not good in this example
        getContentPane().add(actionPane);
        JPanel buttonPane = new JPanel();
        JButton button = new JButton("OK");
        buttonPane.add(button);
        button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				gsm.performEvent(Transition.EXIT_POPUP);
				setVisible(false);
		        dispose();
			}
		});
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
		
	}

}

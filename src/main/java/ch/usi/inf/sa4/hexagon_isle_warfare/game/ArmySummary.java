package ch.usi.inf.sa4.hexagon_isle_warfare.game;

import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Building;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;

public class ArmySummary {
	private final Player player;

	public ArmySummary(Player player) {
		this.player = player;
	}
	
	@Override
	public String toString() {
		String entities_summary = new String();
		for (Unit unit : player.getUnitList()) {
			entities_summary += unit.getName() + "\n";
		}
		
		for (Building building : player.getBuildingList()) {
			entities_summary += building.getName() + "\n";
		}
		return entities_summary;
	}
}

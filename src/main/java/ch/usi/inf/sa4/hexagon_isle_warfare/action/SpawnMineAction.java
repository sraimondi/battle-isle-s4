package ch.usi.inf.sa4.hexagon_isle_warfare.action;

import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;
import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;

public class SpawnMineAction extends Action{

	public SpawnMineAction(final Player owner, final Hexagon target) {
		super(owner, Constants.SPAWNMINEACTIONLENGTH, Constants.SPAWNMINEACTIONPOINTCOST, target);
		// TODO Auto-generated constructor stub
	}

}

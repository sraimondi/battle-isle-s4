package ch.usi.inf.sa4.hexagon_isle_warfare.map;

public class TerrainWeightRetriver {

	static public int terrainWeight(final GroundTypes terrainType) {
		int weight = 0;
		switch (terrainType) {
		case DEEPWATER:
			weight = 1;
			break;
		case MEDIUMWATER:
			weight = 1;
			break;
		case SHALLOWWATER:
			weight = 1;
			break;
		case SAND:
			weight = 1;
			break;
		case ROAD:
			weight = 0;
			break;
		case PLAIN:
			weight = 1;
			break;
		case HILL:
			weight = 1;
			break;
		case MOUNTAIN:
			weight = 2;
			break;
		case ENERGYCELL:
			weight = 1;
			break;

		default:
			break;
		}
		return weight;
	}
}

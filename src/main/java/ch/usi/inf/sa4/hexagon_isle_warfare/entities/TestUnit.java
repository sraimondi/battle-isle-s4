package ch.usi.inf.sa4.hexagon_isle_warfare.entities;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public class TestUnit extends Unit {
	

	public TestUnit(final Integer health,
					final Integer missChance, 
					final Player owner, 
					final Hexagon cell, 
					final Integer movementRange, 
					final Integer attackDamage, 
					final Integer attackRange, 
					final String name,
					final ConcreteTypes unitType){
		super(health, missChance, owner, cell, movementRange, attackDamage, attackRange, name, unitType);
	}

	@Override
	public void draw(Graphics2D g) {
		Vector2D vertices[] = this.getCell().getVertices();
		int xPoly[] = new int[6];
		int yPoly[] = new int[6];
		// Center colored
		Polygon p = new Polygon();
		for (int vertex = 0; vertex < vertices.length; vertex++) {
			xPoly[vertex] = (int) vertices[vertex].getX();
			yPoly[vertex] = (int) vertices[vertex].getY();
			p.addPoint((int) vertices[vertex].getX(),
					(int) vertices[vertex].getY());
		}
		// Draw center
		g.setColor(Constants.UNITCOLOR);
		g.fillPolygon(p);
		// Draw border
		g.setColor(Constants.UNITCOLOR);
		g.drawPolygon(xPoly, yPoly, xPoly.length);
		
	}

	@Override
	public void setColors(Color... newColor) {
		// TODO Auto-generated method stub
		
	}

}


package ch.usi.inf.sa4.hexagon_isle_warfare.map.world;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Building;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.ConcreteTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.HQ;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;
import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.GroundTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.Map;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.SuperGroundTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.TerrainColor;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.strategy.HexagonalStrategy;
import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.perlin_noise.PerlinNoise;

/**
 * @author Simone Raimondi Created on 27/02/14. World class responsible to
 *         create the board and to call the draw method
 */
public class World extends JPanel {

	private Map<Hexagon> board;
	private PerlinNoise mapNoise;
	private final List<Player> playerReference;

	public World(final List<Player> players) {
		this.playerReference = players;
		regenerate();
	}

	public void scale() {
		for (int row = 0; row < Constants.MAP_HEIGHT; row++)
			for (int col = 0; col < Constants.MAP_WIDTH; col++) {
				final double x = col * Constants.HORIZONTAL_DRAW_OFFSET
						+ Constants.MAP_HORIZONTAL_OFFSET;
				final double y = (Constants.VERTICAL_DRAW_OFFSET_0 * row
						+ (col % 2) * Constants.VERTICAL_DRAW_OFFSET_1 + Constants.MAP_VERTICAL_OFFSET);
				board.getCellAt(row, col).updateCenterSize(new Vector2D(x, y),
						Constants.HEXAGON_SIZE);
			}
		repaint();

	}

	public Map<Hexagon> getMap() {
		return board;
	}

	public void regenerate() {
		mapNoise = new PerlinNoise(Constants.PERLIN_HEIGHT,
				Constants.PERLIN_WIDTH, true, Constants.GROUND_PERCENT);
		// Get data form perlin noise
		double maxNoise = mapNoise.getMax();
		double minNoise = mapNoise.getMin();
		double range = maxNoise - minNoise;
		double colorStep = range / 7;
		double deepWaterLevel = minNoise + colorStep;
		double mediumWaterLevel = minNoise + 2 * colorStep;
		double shallowWaterLevel = minNoise + 3 * colorStep;
		double sandLevel = minNoise + 4 * colorStep;
		double plainLevel = minNoise + 5 * colorStep;
		double hillLevel = minNoise + 6 * colorStep;

		// This value is for the number of energy cell to be spawned random
		final int numberOfEnergyCell = (int) (Constants.MAP_WIDTH
				* Constants.MAP_HEIGHT * 0.01);

		try {
			board = new Map<>(Hexagon.class, Constants.MAP_WIDTH,
					Constants.MAP_HEIGHT, new HexagonalStrategy<Hexagon>());
			for (int row = 0; row < Constants.MAP_HEIGHT; row++)
				for (int col = 0; col < Constants.MAP_WIDTH; col++) {
					final double x = col * Constants.HORIZONTAL_DRAW_OFFSET
							+ Constants.MAP_HORIZONTAL_OFFSET;
					final double y = (Constants.VERTICAL_DRAW_OFFSET_0 * row
							+ (col % 2) * Constants.VERTICAL_DRAW_OFFSET_1 + Constants.MAP_VERTICAL_OFFSET);

					// Set terrain type
					if (mapNoise.getPerlinNoise()[(int) y][(int) x] < deepWaterLevel) {
						board.setCell(
								row,
								col,
								new Hexagon(
										new Vector2D(x, y),
										Constants.HEXAGON_SIZE,
										GroundTypes.DEEPWATER,
										TerrainColor
												.retriveColor(GroundTypes.DEEPWATER)));
					} else if (mapNoise.getPerlinNoise()[(int) y][(int) x] < mediumWaterLevel) {
						board.setCell(
								row,
								col,
								new Hexagon(
										new Vector2D(x, y),
										Constants.HEXAGON_SIZE,
										GroundTypes.MEDIUMWATER,
										TerrainColor
												.retriveColor(GroundTypes.MEDIUMWATER)));
					} else if (mapNoise.getPerlinNoise()[(int) y][(int) x] < shallowWaterLevel) {
						board.setCell(
								row,
								col,
								new Hexagon(
										new Vector2D(x, y),
										Constants.HEXAGON_SIZE,
										GroundTypes.SHALLOWWATER,
										TerrainColor
												.retriveColor(GroundTypes.SHALLOWWATER)));
					} else if (mapNoise.getPerlinNoise()[(int) y][(int) x] < sandLevel) {
						board.setCell(row, col, new Hexagon(new Vector2D(x, y),
								Constants.HEXAGON_SIZE, GroundTypes.SAND,
								TerrainColor.retriveColor(GroundTypes.SAND)));
					} else if (mapNoise.getPerlinNoise()[(int) y][(int) x] < plainLevel) {
						board.setCell(row, col, new Hexagon(new Vector2D(x, y),
								Constants.HEXAGON_SIZE, GroundTypes.PLAIN,
								TerrainColor.retriveColor(GroundTypes.PLAIN)));
					} else if (mapNoise.getPerlinNoise()[(int) y][(int) x] < hillLevel) {
						board.setCell(row, col, new Hexagon(new Vector2D(x, y),
								Constants.HEXAGON_SIZE, GroundTypes.HILL,
								TerrainColor.retriveColor(GroundTypes.HILL)));
					} else {
						board.setCell(
								row,
								col,
								new Hexagon(
										new Vector2D(x, y),
										Constants.HEXAGON_SIZE,
										GroundTypes.MOUNTAIN,
										TerrainColor
												.retriveColor(GroundTypes.MOUNTAIN)));
					}
				}

			// % of the map size for energy. Inside ground set the cells energy
			// using random column and random row
			placeEnergy(numberOfEnergyCell);
		} catch (InvalidInputException ex) {
			System.out.println(ex.getMessage());
		}
		cleanMap();
		
		//Clear players units and buildings
		for (Player player : playerReference) {
			player.clear();
		}
		spawnHQ();
		repaint();
	}

	/**
	 * Search for alone cells of TERRAIN with all theirs neighbors of type
	 * WATER. Change the type of these to WATER.
	 * 
	 * @author matteo.muscella@usi.ch
	 */
	private void cleanMap() {
		for (int row = 0; row < Constants.MAP_HEIGHT; row++) {
			for (int col = 0; col < Constants.MAP_WIDTH; col++) {

				final Hexagon cell = board.getCellAt(row, col);
				final HashMap<Integer, Hexagon> neighbors = board
						.getNeighbours(row, col);

				boolean aloneCell = true;
				if (cell.getSuperType() == SuperGroundTypes.GROUND) {
					for (int i = 0; i < 6; i++) {
						if (neighbors.get(i) != null
								&& neighbors.get(i).getSuperType() != SuperGroundTypes.WATER)
							aloneCell &= false;
					}
					if (aloneCell)
						cell.setType(GroundTypes.SHALLOWWATER);
				}
			}
		}
	}

	private void drawWorld(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		board.draw(g2d);
		for (Player player : playerReference) {
			//Draw units
			for (Unit unit : player.getUnitList()) {
				unit.draw(g2d);
			}
			//Draw buildings
			for (Building building : player.getBuildingList()) {
				building.draw(g2d);
			}
		}
	}

	// This method is used to place energy cell on the map.
	public void placeEnergy(int number) {
		for (int i = 0; i < number; i++) {
			Random random = new Random();
			int row = random.nextInt(Constants.MAP_HEIGHT);
			int col = random.nextInt(Constants.MAP_WIDTH);
			Hexagon cell = board.getCellAt(row, col);
			cell.setType(GroundTypes.ENERGYCELL);
			cell.setColors(TerrainColor.retriveColor(cell.getType()));
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawWorld(g);
	}

	public void handleClick(final Vector2D position) {
		for (int row = 0; row < board.getHeight(); row++)
			for (int col = 0; col < board.getWidth(); col++) {
				try {
					if (board.getCellAt(row, col).checkPointInside(position)) {
						board.getCellAt(row, col).handleAction();
						repaint();
					}
				} catch (InvalidInputException ex) {

				}
			}
	}
	
	public Map<Hexagon> getBoard() {
		return board;
	}
	
	/**
	 * 
	 * @param playerList
	 */
	public void spawnHQ() {
		final ArrayList<ConcreteTypes> concreteTypes = new ArrayList<>();
		concreteTypes.add(ConcreteTypes.TEST_UNIT);
		final Random random =  new Random();
		Integer row;
		Integer col;
		for(Player player : playerReference){
			row = random.nextInt(Constants.MAP_HEIGHT);
			col = random.nextInt(Constants.MAP_WIDTH);
			while(!board.getCellAt(row, col).getType().equals(GroundTypes.PLAIN)){
				row = random.nextInt(Constants.MAP_HEIGHT);
				col = random.nextInt(Constants.MAP_WIDTH);
			}
			player.getBuildingList().add(new HQ(20, 0, player, board.getCellAt(row, col), concreteTypes, "HQ", ConcreteTypes.HQ));
		}
	}

	public Hexagon energyCellClick(final Vector2D mouseClick) {
		for (int row = 0; row < board.getHeight(); row++)
			for (int col = 0; col < board.getWidth(); col++) {
				try {
					if (board.getCellAt(row, col).checkPointInside(mouseClick) && board.getCellAt(row, col).getType() == GroundTypes.ENERGYCELL) {
						return board.getCellAt(row, col);
					}
				} catch (IndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidInputException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		return null;
	}
}

package ch.usi.inf.sa4.hexagon_isle_warfare.utilities.quad_tree;

import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.quad_tree.bounds.QuadRect;

/**
 * @author Simone Raimondi
 * Created on 03/03/14.
 */
public interface QuadTreeElement {
    /**
     * Method to check is inside a rectangular bounds
     * @param bounds The rectangular bounds where the object has to fit
     * @return 0 object is not inside bounds, 1 if the object completely fits in the bounds
     */
    public boolean isInside(final QuadRect bounds);
}

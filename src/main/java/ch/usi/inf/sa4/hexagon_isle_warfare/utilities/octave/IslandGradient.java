package ch.usi.inf.sa4.hexagon_isle_warfare.utilities.octave;

/**
 * @author Simone Raimondi
 *         Created on 12/03/14.
 */
public class IslandGradient {
    private final double noise[][];

    public IslandGradient(final int height, final int width, final double islandSize) {
        noise = generateGradient(height, width, islandSize);
    }

    private double[][] generateGradient(final int height, final int width, final double islandSize) {

        double gradientNoise[][] = new double[height][width];

        final int xCenter = width / 2;
        final int yCenter = height / 2;
        final double maxDistance = Math.min(yCenter, xCenter);


        for (int row = 0; row < height; row++)
            for (int col = 0; col < width; col++) {
                double distance = Math.sqrt(Math.pow(row - yCenter, 2) + Math.pow(col - xCenter, 2));
                double value = (1.0d - distance / (maxDistance * islandSize * 1.4));
                if (value < 0.0d) value = 0.0d;
                gradientNoise[row][col] = value;
            }

        return gradientNoise;
    }


    public double[][] getNoise() {
        return noise;
    }
}

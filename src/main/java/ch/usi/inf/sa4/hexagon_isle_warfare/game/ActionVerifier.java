package ch.usi.inf.sa4.hexagon_isle_warfare.game;

import java.util.HashMap;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.action.Action;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.AttackAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.ConquerAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.MoveAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.SpawnAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.SpawnMineAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.AvailableTerrainsRetriver;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Building;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;
import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.GroundTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.Map;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public class ActionVerifier {

	private final Map<Hexagon> worldMap;
	private final List<Player> playerList;
	
	
	public ActionVerifier(Map<Hexagon> worldMap, List<Player> playerList){
		this.worldMap = worldMap;
		this.playerList = playerList;
	}
	
	
	public boolean validateAction(final Action action){
		if(action instanceof AttackAction){
			return validateAttackAction((AttackAction) action);
		}
		else if(action instanceof MoveAction){
			return validateMoveAction((MoveAction) action);
		}
		else if(action instanceof SpawnAction){
			return validateSpawnAction((SpawnAction) action);
		}
		else if(action instanceof ConquerAction){
			return validateConquerAction((ConquerAction) action);
		}
		else if(action instanceof SpawnMineAction){
			return validateSpawnMineAction((SpawnMineAction) action);
		}
		return false;
	}
	
	private boolean validateSpawnMineAction(final SpawnMineAction action) {
		if(action.getTarget().getType().equals(GroundTypes.ENERGYCELL)){
			for(Player player : playerList){
				if(action.getOwner().equals(player)) {
					for(Unit unit : player.getUnitList()) {
						if(unit.getCell().equals(action.getTarget())){
							System.out.println("Action Rejected");
							return false;
						}
					}
					for(Building building : player.getBuildingList()) {
						if(building.getCell().equals(action.getTarget())){
							System.out.println("Action Rejected");
							return false;
						}
					
					}
				}
			}
			System.out.println("Action accepted");
			return true;
		}
		System.out.println("Action Rejected");
		return false;
	}


	private boolean validateAttackAction(final AttackAction action){
		final Player owner = action.getOwner();
		if(owner.getActionPoint() > action.getActionPointCost()){
			for(Player player : playerList){
				if(!owner.equals(player)) {
					for(Unit unit : player.getUnitList()) {
						if(unit.getCell().equals(action.getTarget())){
							//TODO add check for attack range
							action.setAttacked(unit);
							owner.setActionPoint(owner.getActionPoint() - action.getActionPointCost());
							System.out.println("Action accepted");
							return true;
						}
					}
					for(Building building : player.getBuildingList()) {
						if(building.getCell().equals(action.getTarget())){
							//TODO add check for attack range
							action.setAttacked(building);
							owner.setActionPoint(owner.getActionPoint() - action.getActionPointCost());
							System.out.println("Action accepted");
							return true;
						}
					}
				}
			}
		}
		System.out.println("Action Rejected");
		return false;
	}
	
	private boolean validateMoveAction(final MoveAction action){
		//TODO Check for design
		//For now considering movement range of one unit=1
		final Player owner = action.getOwner();
		if(owner.getActionPoint() > action.getActionPointCost()){
			if(!AvailableTerrainsRetriver.availableTerrain(action.getActor().getUnitType()).contains(action.getTarget().getType())){
				try {
					final HashMap<Integer, Hexagon> neighbours = worldMap.getNeighbours(action.getActor().getCell());
					if(neighbours.containsValue(action.getTarget())){
						owner.setActionPoint(owner.getActionPoint() - action.getActionPointCost());
						System.out.println("Action accepted");
						return true;
					}
				} catch(InvalidInputException ex) {
					System.out.println(ex.getMessage());
					System.out.println("Action Rejected");
					return false;
				}
			}
		}
		System.out.println("Action Rejected");
		return false;
	}
	
	private boolean validateSpawnAction(final SpawnAction action){
		
		final Player owner = action.getOwner();
		if(owner.getActionPoint() > action.getActionPointCost() && owner.getEnergy()>action.getEnergyCost()){
			if(AvailableTerrainsRetriver.availableTerrain(action.getName()).contains(action.getTarget().getType())) {
				try {
					final HashMap<Integer, Hexagon> neighbours = worldMap.getNeighbours(action.getActor().getCell());
					if(neighbours.containsValue(action.getTarget())){
						for(Player player : playerList){
								for(Unit unit : player.getUnitList()) {
									if(unit.getCell().equals(action.getTarget())){
										//TODO add check for attack range
										System.out.println("Action Rejected");
										return false;
									}
								}
								for(Building building : player.getBuildingList()) {
									if(building.getCell().equals(action.getTarget())){
										//TODO add check for attack range
										System.out.println("Action Rejected");
										return false;
									}
								}
						}
						owner.setActionPoint(owner.getActionPoint() - action.getActionPointCost());
						owner.setEnergy(owner.getEnergy() - action.getEnergyCost());
						System.out.println("Action accepted");
						return true;
					}
				} catch(InvalidInputException ex) {
					System.out.println(ex.getMessage());
					System.out.println("Action Rejected");
					return false;
				}
			}
		}
		System.out.println("Action Rejected");
		return false;
	
	}
	
	private boolean validateConquerAction(final ConquerAction action){
		final Player owner = action.getOwner();
		if(owner.getActionPoint() > action.getActionPointCost()){
			for(Player player : playerList){
				if(!owner.equals(player)) {
					for(Building building : player.getBuildingList()) {
						if(building.getCell().equals(action.getTarget())){
							//TODO add check for attack range
							action.setConquered(building);
							owner.setActionPoint(owner.getActionPoint() - action.getActionPointCost());
							System.out.println("Action accepted");
							return true;
						}
					}
				}
			}
		}
		System.out.println("Action rejected");
		return false;
	}
	
}
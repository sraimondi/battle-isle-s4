package ch.usi.inf.sa4.hexagon_isle_warfare.action;
import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Building;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.ConcreteTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public class SpawnAction extends Action {
	
	private final ConcreteTypes name;
	private final Building actor;
	private final Integer energyCost;
	
	public SpawnAction(final Player owner, final Building actor, final Hexagon target, final ConcreteTypes name, final Integer energyCost){
		super(owner, Constants.SPAWNACTIONLENGTH, Constants.SPAWNACTIONPOINTCOST, target);
		this.name = name;
		this.energyCost = energyCost;
		this.actor = actor;
		
	}

	public ConcreteTypes getName() {
		return name;
	}

	public Integer getEnergyCost() {
		return energyCost;
	}

	public Building getActor() {
		return actor;
	}
	

}

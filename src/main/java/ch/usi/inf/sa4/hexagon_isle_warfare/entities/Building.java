package ch.usi.inf.sa4.hexagon_isle_warfare.entities;


import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public abstract class Building extends Entity {
	
	protected ArrayList<Unit> unitsInside;
	
	public Building(final Integer health, final Integer missChance, final Player owner, final Hexagon cell, final String name, final ConcreteTypes unitType){
		super(health, missChance, owner, cell, name, unitType);
		this.unitsInside = new ArrayList<>();
	}
	
	public void setOwner(Player newOwner){
		this.owner = newOwner;
	}
	
	public void enterBuilding(Unit unit){
		unitsInside.add(unit);
	}
	
	public void leaveBuilding(Unit unit){
		unitsInside.remove(unit);
	}
	
	public abstract List<String> retriveActions ();
	
}

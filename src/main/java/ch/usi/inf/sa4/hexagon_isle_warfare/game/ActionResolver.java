package ch.usi.inf.sa4.hexagon_isle_warfare.game;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.action.Action;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.AttackAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.ConquerAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.MoveAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.SpawnAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.action.SpawnMineAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Building;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.ConcreteTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.HQ;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Mine;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.TestUnit;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;

/**
 * 
 * @author Matteo Morisoli
 *
 */
public class ActionResolver {
	
	/**
	 * 
	 */
	private final List<Player> playerList;
	private final List<List<Action>> actionLists;
	private final List<Action> actionSequence;
	
	/**
	 * 
	 */
	public ActionResolver(final List<Player> playerlist){
		this.actionLists = new ArrayList<>();
		this.actionSequence = new ArrayList<>();
		this.playerList = playerlist;
	}
	
	/**
	 * 
	 * @param actions
	 */
	public void addPlayerActions(){
		for(Player player : playerList)
			this.actionLists.add(player.getActionList());
	}
	
	/**
	 * 
	 */
	public void emptyLists(){
		for(Player player : playerList){
			player.clearActionList();
		}
		this.actionLists.clear();
	}
	
	/**
	 * 
	 */
	public void resolveActions(){
		int maxLength = computeLongestActionListLength();
		//TODO fix this orrible thing
		for(int i = 0; i < maxLength; i++){
			for(int j = 0; j < 10; j++){
				for(List<Action> actionList : actionLists){
					if(actionList.size() - 1 >= i){
						if(actionList.get(i).getLength() == j){
							actionSequence.add(actionList.get(i));
							selectResolver(actionList.get(i));
						}
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @param action
	 */
	public void selectResolver(Action action){
		if(action instanceof AttackAction){
			resolveAttackAction((AttackAction) action);
		}
		else if(action instanceof MoveAction){
			resolveMoveAction((MoveAction) action);
		}
		else if(action instanceof SpawnAction){
			resolveSpawnAction((SpawnAction) action);
		}
		else if(action instanceof ConquerAction){
			resolveConquerAction((ConquerAction) action);
		}
		else if(action instanceof SpawnMineAction){
			resolveSpawnMineAction((SpawnMineAction) action);
		}
	}
	
	/**
	 * 
	 * @param move
	 */
	public void resolveMoveAction(MoveAction move){
		move.getActor().setCell(move.getTarget());
		for(Player player : this.playerList){
			if(!move.getOwner().equals(player)){
				for(Unit unit : player.getUnitList()){
					if(unit.getCell().equals(move.getTarget()))
						resolveBattle(move.getActor(), unit);
				}
			}
		}
	}
	
	private void resolveBattle(final Unit actor, final Unit unit) {
		System.out.println("BATTLE!");
		if(Math.random() > 0.5){
			unit.getOwner().getUnitList().remove(unit);
		}
		else{
			actor.getOwner().getUnitList().remove(actor);
		}
	}

	/**
	 * 
	 * @param attack
	 */
	public void resolveAttackAction(AttackAction attack){
		attack.getAttacked().setHealth(attack.getAttacked().getHealth() - attack.getActor().getAttackDamage());
		if(attack.getAttacked().getHealth() <= 0){
			if(attack.getAttacked() instanceof HQ){
				//TODO call GG
			}
			attack.getAttacked().getOwner().getUnitList().remove(attack.getAttacked());
		}
	}
	
	/**
	 * 
	 * @param conquer
	 */
	public void resolveConquerAction(ConquerAction conquer){
		conquer.getConquered().setOwner(conquer.getOwner());
	}
	
	/**
	 * 
	 * @param spawn
	 */
	public void resolveSpawnAction(SpawnAction spawn){
		Unit newUnit = new TestUnit(10, 0, spawn.getOwner(), spawn.getTarget(), 1, 5, 5, "testUnit", ConcreteTypes.TEST_UNIT);
		spawn.getOwner().getUnitList().add(newUnit);
	}
	
	/**
	 * 
	 * @param spawnMine
	 */
	public void resolveSpawnMineAction(SpawnMineAction spawnMine){
		Building newMine = new Mine(10, 0, spawnMine.getOwner(), spawnMine.getTarget(), "mine", ConcreteTypes.MINE);
		spawnMine.getOwner().getBuildingList().add(newMine);
	}
	
	/**
	 * 
	 * @return
	 */
	public int computeLongestActionListLength(){
		int maxLength = 0;
		for(List<Action> actions : actionLists){
			int size = actions.size();
			if(size > maxLength){
				maxLength = size;
			}
		}
		return maxLength;
	}
		
}

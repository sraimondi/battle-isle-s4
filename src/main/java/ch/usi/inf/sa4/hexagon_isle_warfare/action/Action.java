package ch.usi.inf.sa4.hexagon_isle_warfare.action;

import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;


public abstract class Action {
	
	private final Player owner;
	protected final Integer length;
	protected final Integer actionPointCost;
	private final Hexagon target;
	
	public Action(final Player owner, final Integer length, final Integer actionPointCost, final Hexagon target){
		this.owner = owner;
		this.length = length;
		this.actionPointCost = actionPointCost;
		this.target = target;
	}

	public Integer getLength() {
		return length;
	}

	public Integer getActionPointCost() {
		return actionPointCost;
	}

	public Hexagon getTarget() {
		return target;
	}

	public Player getOwner() {
		return owner;
	}
	
}

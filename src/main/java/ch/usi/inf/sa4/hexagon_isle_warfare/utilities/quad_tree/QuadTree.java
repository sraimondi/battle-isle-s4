package ch.usi.inf.sa4.hexagon_isle_warfare.utilities.quad_tree;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.quad_tree.bounds.QuadRect;

/**
 * @author Simone Raimondi
 * Created on 03/03/14.
 */
public class QuadTree <E extends QuadTreeElement> {
    //Constants variable
    private int MAX_CAPACITY = 5;
    private int MAX_LEVELS = 10;

    //Quadtree variables
    final private int level;
    final private QuadRect bounds;
    final private List<E> elements;
    /**
     * Children order
     * ---------
     * | 1 | 0 |
     * ---------
     * | 2 | 3 |
     * ---------
     */
    final private QuadTree<E>[] children;

    /**
     * Default constructor for the quadtree
     * @param level Initial deep level
     * @param bounds Initial bounds
     */
    @SuppressWarnings({"unchecked"})
    public QuadTree(final int level, final QuadRect bounds) {
        this.level = level;
        this.bounds = bounds;
        elements = new ArrayList<>();
        children = new QuadTree[4];
    }

    /**
     * Clear the elements array
     */
    public void clear() {
        elements.clear();
        for (int i = 0; i < children.length; i++) {
            if (children[i] != null) {
                children[i].clear();
                children[i] = null;
            }
        }
    }


    /**
     * Splits the current nod in four subnodes
     */
    private void split() {
        final int subWidth = bounds.getWidth() / 2;
        final int subHeight = bounds.getHeight() / 2;

        final int xTopLeftCorner = (int)bounds.getTopLeftCorner().getX();
        final int yTopLeftCorner = (int)bounds.getTopLeftCorner().getY();

        children[0] = new QuadTree<>(level + 1, new QuadRect(new Vector2D(xTopLeftCorner + subWidth, yTopLeftCorner), subHeight, subWidth));
        children[1] = new QuadTree<>(level + 1, new QuadRect(new Vector2D(xTopLeftCorner, yTopLeftCorner), subHeight, subWidth));
        children[2] = new QuadTree<>(level + 1, new QuadRect(new Vector2D(xTopLeftCorner, yTopLeftCorner + subHeight), subHeight, subWidth));
        children[3] = new QuadTree<>(level + 1, new QuadRect(new Vector2D(xTopLeftCorner + subWidth, yTopLeftCorner + subHeight), subHeight, subWidth));

    }

    /**
     * This method computed where the object must go for the children of the current node
     * @param object The object that you want to insert
     * @return The index of the child where you have to add the object, returns -1 if the object does not fit completely in one quadrant and it must go in the parent list of elements
     */
    private int getQuadrantIndex(final E object) {
        int index = -1;
        for (int i = 0; i < children.length; i++) if (children[i].bounds.contains(object)) index = i;
        return index;
    }

    /**
     * This method is responsible to add an object to the quadtree
     * @param objectToInsert The object you want to insert
     */
    @SuppressWarnings({"unchecked"})
    public void insert(final E objectToInsert) {
        if (children[0] != null) {
            int index = getQuadrantIndex(objectToInsert);

            if (index != -1) {
                children[index].insert(objectToInsert);
                return;
            }
        }

        elements.add(objectToInsert);

        if (elements.size() > MAX_CAPACITY && level < MAX_LEVELS) {
            if (children[0] == null) split();

            int j = 0;
            while (j < elements.size()) {
                int index = getQuadrantIndex(elements.get(j));
                if (index != -1) children[index].insert(elements.remove(j));
                else j++;
            }
        }
    }

    /**
     * Draw the quadtree
     * @param g The graphics where to draw the quad tree
     */
    public void draw(Graphics g) {
        if (children[0] != null) for (QuadTree<E> aChildren : children) aChildren.draw(g);
        g.setColor(Color.BLACK);
        g.drawRect((int)bounds.getTopLeftCorner().getX(), (int)bounds.getTopLeftCorner().getY(), bounds.getWidth(), bounds.getHeight());


    }

}

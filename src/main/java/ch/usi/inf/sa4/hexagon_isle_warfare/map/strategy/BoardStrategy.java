package ch.usi.inf.sa4.hexagon_isle_warfare.map.strategy;

import java.util.HashMap;

import ch.usi.inf.sa4.hexagon_isle_warfare.event_listener.EventListener;
import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.graphic.Drawable;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.Map;
import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.ArrayCoordinate;

public interface BoardStrategy<T extends Drawable & EventListener> {
	
	/**
    * @param boardReference the board to use to compute the neighbours
    * @param row the row of the cell you want to get the neighbours
    * @param col the column of the cell you want to get the neighbours
    * @return an hashmap with all the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
    * If a cell does not have a neighbour then the value is null
    */
	public abstract HashMap<Integer, T> getNeighbours(final Map<T> boardReference, final int row, final int col) throws IndexOutOfBoundsException;
	
	/**
    * @param boardReference the board to use to compute the neighbours
    * @param row the row of the cell you want to get the neighbours
    * @param col the column of the cell you want to get the neighbours
    * @return an hashmap with all the coordinates of the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
    * If a cell does not have a neighbour then the value is null
    */
	public abstract HashMap<Integer, ArrayCoordinate> getNeighboursCoordinates(final Map<T> boardReference, final int row, final int col) throws IndexOutOfBoundsException;
	
	/**
	 * 
	 * @param boardReference the board to use to compute the neighbours
	 * @param cell the cell that you want to get the neighbour
	 * @returnan hashmap with all the coordinates of the neighbours, sorted such that the one with index 0 is the one on top and the order is clockwise.
     * If a cell does not have a neighbour then the value is null
	 */
	public abstract HashMap<Integer, T> getNeighbour(final Map<T> boardReference, final T cell) throws InvalidInputException;
}

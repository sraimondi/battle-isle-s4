package ch.usi.inf.sa4.hexagon_isle_warfare.constants;

import java.awt.*;

/**
 * @author Simone Raimondi Created on 28/02/14.
 */
public class Constants {
	static public int WINDOW_H = 900;
	static public int WINDOW_W = 1000;
	static public int OFFSET_WATER = 50;
	static public int MAP_WIDTH =30;
	static public int MAP_HEIGHT = 25;
	static public double GROUND_PERCENT = 0.7;
	static public double MAP_VERTICAL_OFFSET = 0;
	static public double MAP_HORIZONTAL_OFFSET = 0;
	static public double HEXAGON_SIZE = computeHexSize();
	static public double HORIZONTAL_DRAW_OFFSET = HEXAGON_SIZE * 3 / 2;
	static public double VERTICAL_DRAW_OFFSET_0 = Math.sqrt(3) / 2
			* HEXAGON_SIZE * 2;
	static public double VERTICAL_DRAW_OFFSET_1 = Math.sqrt(3) / 2
			* HEXAGON_SIZE;
	static public Color DEF_COLOR = new Color(0, 0, 255);
	static public Color SELECTED_COLOR = new Color(175, 238, 238);
	static public Color NEIGHBOUR_COLOR = new Color(0, 191, 255);
	static public Color N_NEIGHBOUR_COLOR = new Color(142, 229, 238);
	static public Color UNITCOLOR = new Color(255,0,0);
	static public Color HQCOLOR = new Color(255,255,255);
	static public Color MINECOLOR = new Color(1,2,3);
	
	static public int PERLIN_WIDTH = (int) ((2 + (MAP_WIDTH - 1) * 3 / 2) * HEXAGON_SIZE);
	static public int PERLIN_HEIGHT = (int) (Math.sqrt(3) * MAP_HEIGHT * HEXAGON_SIZE);
	
	/*
	 * Action costs
	 */
	static public final Integer SPAWNACTIONLENGTH = 4;
	static public final Integer SPAWNACTIONPOINTCOST = 2;
	static public final Integer MOVEACTIONLENGTH = 3;
	static public final Integer MOVEACTIONPOINTCOST = 3;
	static public final Integer ATTACKACTIONLENGTH = 2;
	static public final Integer ATTACKACTIONPOINTCOST = 4;
	static public final Integer CONQUERACTIONLENGTH = 5;
	static public final Integer CONQUERACTIONPOINTCOST = 5;
	static public final Integer SPAWNMINEACTIONLENGTH = 4;
	static public final Integer SPAWNMINEACTIONPOINTCOST = 2;
	
	/*
	 * basic game parameters
	 */
	static public final Integer BASEENERGYGAIN = 100;
	static public final Integer BASEACTIONPOINTGAIN = 10;
	static public final Integer MINEPRODUCTIONENERGY = 30;
	/*
	 * static public Color SELECTED_COLOR = new Color(175, 238, 238); static
	 * public Color NEIGHBOUR_COLOR = new Color(0, 191, 255); static public
	 * Color N_NEIGHBOUR_COLOR = new Color(142, 229, 238);
	 */
	private static double computeHexSize() {
		return Math.min(WINDOW_W / (2 + ((MAP_WIDTH - 1) * 3.0 / 2.0)), WINDOW_H
				/ (Math.sqrt(3) * MAP_HEIGHT));
	}

	public static void update(final int wW, final int wH, final int width, final int height,
			final double percentage) {
		WINDOW_W = wW;
		WINDOW_H = wH;
		MAP_WIDTH = width;
		MAP_HEIGHT = height;
		GROUND_PERCENT = percentage;
		HEXAGON_SIZE = computeHexSize();
		OFFSET_WATER = (int) HEXAGON_SIZE * 2;
		HORIZONTAL_DRAW_OFFSET = HEXAGON_SIZE * 3 / 2;
		VERTICAL_DRAW_OFFSET_0 = Math.sqrt(3) / 2 * HEXAGON_SIZE * 2;
		VERTICAL_DRAW_OFFSET_1 = Math.sqrt(3) / 2 * HEXAGON_SIZE;
		PERLIN_WIDTH = (int) ((2 + (MAP_WIDTH - 1) * 3 / 2) * HEXAGON_SIZE);
		PERLIN_HEIGHT = (int) (Math.sqrt(3) * MAP_HEIGHT * HEXAGON_SIZE);
	}
}
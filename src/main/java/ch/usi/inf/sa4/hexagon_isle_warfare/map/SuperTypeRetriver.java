package ch.usi.inf.sa4.hexagon_isle_warfare.map;

public class SuperTypeRetriver {
	
	/**
	 * Method for retrieving the superTerrainType based on the terrain type.
	 * @param type The terrain type.
	 * @return return SuperGroundTypes The superTerrainType of that type of terrain.
	 */
	public static SuperGroundTypes getTerrainSuperType(final GroundTypes terrainType) {
		
		SuperGroundTypes superType;
		switch (terrainType) {
		case DEEPWATER:
			superType = SuperGroundTypes.WATER;
			break;
		case MEDIUMWATER:
			superType = SuperGroundTypes.WATER;
			break;
		case SHALLOWWATER:
			superType = SuperGroundTypes.WATER;
			break;
		case SAND:
			superType = SuperGroundTypes.GROUND;
			break;
		case ROAD:
			superType = SuperGroundTypes.GROUND;
			break;
		case PLAIN:
			superType = SuperGroundTypes.GROUND;
			break;
		case HILL:
			superType = SuperGroundTypes.GROUND;
			break;
		case MOUNTAIN:
			superType = SuperGroundTypes.GROUND;
			break;
		case ENERGYCELL:
			superType = SuperGroundTypes.GROUND;
			break;
	
		default:
			System.out.println("No super ground type found for terrain given");
			superType = null;
			break;
		}
		return superType;
	}
}

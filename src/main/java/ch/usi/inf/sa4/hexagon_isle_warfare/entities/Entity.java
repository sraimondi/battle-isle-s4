package ch.usi.inf.sa4.hexagon_isle_warfare.entities;

import java.awt.Color;
import java.awt.Graphics2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.graphic.Drawable;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

/**
 * 
 * @author Matteo Morisoli
 * The abstract class Entity is the top level class of the entity hierarchy, and defines the basic attributes of every entity in the field
 */
public abstract class Entity implements Drawable {
	/**
	 * 
	 */
	protected Integer health;
	protected Integer missChance;
	//TODO Check for design
	protected Player owner;
	protected Hexagon cell;
	protected String name;
	private final ConcreteTypes unitType;
	
	/**
	 * 
	 * @param health this parameter is the starting health of an entity
	 * @param missChance this parameter is the probability of the entity to avoid an incoming attack
	 * @param owner this is the Player who controls the entity
	 * @param cell the Hexagon where the unit currently is
	 * @param name the unit type name
	 */
	public Entity(final Integer health, final Integer missChance, final Player owner, final Hexagon cell, final String name, ConcreteTypes unitType){
		this.health = health;
		this.missChance = missChance;
		this.owner = owner;
		this.setCell(cell);
		this.name = name;
		this.unitType = unitType;
	}
	
	public Integer getHealth() {
		return health;
	}
	
	public void setHealth(Integer health) {
		this.health = health;
	}
	
	public Integer getMissChance() {
		return missChance;
	}
	
	public Player getOwner() {
		return owner;
	}

	public String getName() {
		return name;
	}

	public Hexagon getCell() {
		return cell;
	}

	public void setCell(Hexagon cell) {
		this.cell = cell;
	}

	public ConcreteTypes getUnitType() {
		return unitType;
	}
}

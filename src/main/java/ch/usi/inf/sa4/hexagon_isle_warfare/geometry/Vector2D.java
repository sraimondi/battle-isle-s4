package ch.usi.inf.sa4.hexagon_isle_warfare.geometry;

import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;

/**
 * @author Simone Raimondi
 * Created on 27/02/14.
 */
public class Vector2D {
    final double xCoord;
    final double yCoord;

    /**
     * Constructor for the Vector2D class
     * @param x the x value for the vector
     * @param y the y value for the vector
     */
    public Vector2D(final double x, final double y) {
        xCoord = x;
        yCoord = y;
    }

    /**
     * Copy constructor of the class
     * @param vectorToCopy The vector that you want to copy
     * @throws InvalidInputException If the input is null the method throws an exception
     */
    public Vector2D(final Vector2D vectorToCopy) throws InvalidInputException {
        if (vectorToCopy == null) throw new InvalidInputException("Argument is null");
        xCoord = vectorToCopy.xCoord;
        yCoord = vectorToCopy.yCoord;
    }

    /**
     *
     * @return The x coordinate of the vector
     */
    public double getX() {
        return xCoord;
    }

    /**
     *
     * @return The y coordinate of the vector
     */
    public double getY() {
        return yCoord;
    }

    /**
     *
     * @return length of the vector
     */
    public double norm() {
        return Math.sqrt(Math.pow(yCoord, 2) + Math.pow(xCoord, 2));
    }

    /**
     *
     * @param point The point for which you want to compute the distance
     * @return The distance to the given point
     * @throws InvalidInputException If the input is null the method throws an exception
     */
    public double distance(final Vector2D point) throws InvalidInputException {
        if (point == null) throw new InvalidInputException("Argument is null");
        return Math.sqrt(Math.pow(point.yCoord - yCoord, 2) + Math.pow(point.xCoord - xCoord, 2));
    }

    /**
     *
     * @param vectorToAdd The vector that you are adding
     * @return A new vector representing the sum of the one you are celling the method from plus the given one
     * @throws InvalidInputException If the input is null the method throws an exception
     */
    public Vector2D add(final Vector2D vectorToAdd) throws InvalidInputException {
        if (vectorToAdd == null) throw new InvalidInputException("Argument is null");
        return new Vector2D(xCoord + vectorToAdd.xCoord, yCoord + vectorToAdd.yCoord);
    }

    /**
     *
     * @param vectorToSub The vector that is subtracted to the current one
     * @return A new vector that represents the subtraction of the given one to this
     * @throws InvalidInputException If the input is null the method throws an exception
     */
    public Vector2D sub(final Vector2D vectorToSub) throws InvalidInputException {
        if (vectorToSub == null) throw new InvalidInputException("Argument is null");
        return new Vector2D(vectorToSub.xCoord - xCoord, vectorToSub.yCoord - yCoord);
    }

    /**
     *
     * @param angle The angle that you want to rotate the vector
     * @return A new vector representing the rotation of this for the given angle
     */
    public Vector2D rotate(final double angle) {
        return new Vector2D(xCoord * Math.cos(angle) - yCoord * Math.sin(angle), xCoord * Math.sin(angle) + yCoord * Math.cos(angle));
    }
}

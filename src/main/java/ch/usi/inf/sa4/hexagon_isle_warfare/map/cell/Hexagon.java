package ch.usi.inf.sa4.hexagon_isle_warfare.map.cell;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.event_listener.EventListener;
import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.graphic.Drawable;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.GroundTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.SuperGroundTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.SuperTypeRetriver;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.TerrainColor;

/**
 * @author Simone Raimondi Created on 27/02/14.
 */
public class Hexagon implements Drawable, EventListener {

	private Vector2D center;
	private double size;
	private Vector2D vertices[];
	private double height;
	private GroundTypes type;
	private Color color;

	/**
	 * Constructor of the hexagon
	 * 
	 * @param center
	 *            Center of the hexagon
	 * @param size
	 *            Size of the hexagon
	 * @param type
	 *            Type of the terrain
	 * @param color
	 *            Color of the hexagon
	 */
	public Hexagon(final Vector2D center, final double size,
			final GroundTypes type, final Color color) {
		this.center = center;
		this.size = size;
		vertices = new Vector2D[6];
		initVertices();
		height = Math.sqrt(3) / 2.0 * size;
		this.type = type;
		this.color = color;
	}

	@Override
	public void draw(Graphics2D g) {
		int xPoly[] = new int[6];
		int yPoly[] = new int[6];
		// Center colored
		Polygon p = new Polygon();
		for (int vertex = 0; vertex < vertices.length; vertex++) {
			xPoly[vertex] = (int) vertices[vertex].getX();
			yPoly[vertex] = (int) vertices[vertex].getY();
			p.addPoint((int) vertices[vertex].getX(),
					(int) vertices[vertex].getY());
		}
		// Draw center
		g.setColor(color);
		g.fillPolygon(p);
		// Draw border
		g.setColor(Color.BLACK);
		g.drawPolygon(xPoly, yPoly, xPoly.length);
	}

	/**
	 * Vertex with index 0 is the one on the left, then increasing counter
	 * clockwise
	 */
	public void initVertices() {
		Vector2D centerToVertex0 = new Vector2D(-size, 0);
		try {
			for (int vertex = 0; vertex < vertices.length; vertex++) {
				vertices[vertex] = center.add(centerToVertex0.rotate(Math
						.toRadians(360.0d / vertices.length) * vertex));
			}
		} catch (InvalidInputException ex) {
			System.out.println(ex.getMessage());
		}

	}

	/**
	 * Getter of the height attribute
	 * 
	 * @return The height attribute
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * Getter for the SuperType of the Hexagon
	 * 
	 * @author matteo.muscella@usi.ch
	 * @return SuperGroundTypes The Super Ground Type
	 */
	public SuperGroundTypes getSuperType() {
		return SuperTypeRetriver.getTerrainSuperType(type);
	}

	/**
	 * Getter of the type attribute
	 * 
	 * @return The type attribute
	 */
	public GroundTypes getType() {
		return type;
	}

	/**
	 * Setter for the type attribute
	 * 
	 * @param newType
	 *            New Terrain type
	 */
	public void setType(final GroundTypes newType) {
		type = newType;
		setColors(TerrainColor.retriveColor(type));
	}

	/**
	 * Update center and size of the hexagon
	 * 
	 * @param newCenter
	 *            The new center of the hexagon
	 * @param newSize
	 *            The new size of the hexagon
	 */
	public void updateCenterSize(final Vector2D newCenter, final double newSize) {
		center = newCenter;
		size = newSize;
		height = Math.sqrt(3) / 2.0 * size;
		initVertices();
	}

	@Override
	public boolean checkPointInside(final Vector2D point)
			throws InvalidInputException {
		// First check if the point is in the central rectangular area
		return center.distance(point) <= (size + height) / 2;

	}

	@Override
	public void setColors(final Color... newColor) {
		color = newColor[0];
	}

	@Override
	public void handleAction() {
		this.color = Constants.SELECTED_COLOR;
		
	}
	
	public Vector2D[] getVertices() {
		return this.vertices;
	}

}

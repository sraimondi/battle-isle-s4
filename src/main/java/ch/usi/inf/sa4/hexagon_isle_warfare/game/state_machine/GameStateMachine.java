package ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine;

import ch.usi.inf.sa4.hexagon_isle_warfare.game.Game;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.states.State;


/**
 * 
 * @author Simone Raimondi
 *
 */

public class GameStateMachine {
	
	private State currentState;
	private Game game;
	
	/**
	 * Default state machine constructor
	 */
	public GameStateMachine(final Game game) {
		currentState = State.WAITING_CLICK;
		this.game = game;
	}
	
	/**
	 * Perform an event on the state machine
	 * @param event Transition to perform
	 */
	public void performEvent(final Transition event) {
		currentState = currentState.nextState(event);
	}
	
	public State currentState() {
		return this.currentState;
	}
}

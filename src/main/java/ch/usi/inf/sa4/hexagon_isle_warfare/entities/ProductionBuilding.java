package ch.usi.inf.sa4.hexagon_isle_warfare.entities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.action.SpawnAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;



public abstract class ProductionBuilding extends Building{
	
	private final List<ConcreteTypes> buildableUnits;

	public ProductionBuilding(final Integer health, final Integer missChance, final Player owner, final Hexagon cell, final List<ConcreteTypes> buildableUnits, final String name, final ConcreteTypes unitType) {
		super(health, missChance, owner, cell, name, unitType);
		this.buildableUnits = buildableUnits;
	}

	public List<ConcreteTypes> getBuildableUnits() {
		return buildableUnits;
	}
	


}

package ch.usi.inf.sa4.hexagon_isle_warfare.map;

/**
 * Different kind of terrain.
 */
public enum GroundTypes {
	DEEPWATER,
	MEDIUMWATER,
	SHALLOWWATER,
	SAND,
	ROAD,
	PLAIN,
	HILL,
	MOUNTAIN,
	ENERGYCELL
}


package ch.usi.inf.sa4.hexagon_isle_warfare.application;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Game;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.world.World;

/**
 * @author Simone Raimondi
 * Created on 27/02/14.
 * Simple application class
 */
public class App extends JFrame implements ComponentListener {
	
	private final Game game;
	private World mapPanel;
	Insets insets;
	
	//Spinner for setting the map criteria
    JSpinner widthField;
    JSpinner heightField;
    JSpinner groundField;
    
    JMenuBar menubar;

    private Integer widthValue = Constants.MAP_WIDTH;
    private Integer heightValue = Constants.MAP_HEIGHT;
    private Double groundValue = Constants.GROUND_PERCENT;

    public App() {
    	game = new Game();
        initUI();
    }

    public final void initUI() {
    	this.addComponentListener(this);
    	this.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				Vector2D mouseClick = new Vector2D(e.getX(), e.getY() - insets.top - menubar.getHeight());
				game.handleClick(mouseClick);
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
        setTitle("CELL OF DUTY map demo");
        //setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(600, 200));

        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        /**Left and Right main panel*/
        JPanel leftToolbarPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(leftToolbarPanel, BoxLayout.X_AXIS); //Top to bottom
        leftToolbarPanel.setLayout(boxLayout);

      
        mapPanel = game.getWorld();
        
        /*Set the layout*/

        /**Width Panel*/
        JPanel widthPanel = new JPanel();
        FlowLayout layout = new FlowLayout();
        widthPanel.setLayout(layout);

        JLabel width = new JLabel("Width");
        widthPanel.add(width);

        widthField = new JSpinner(new SpinnerNumberModel(Constants.MAP_WIDTH,20,300,5));
        widthField.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                widthValue = new Integer((int)widthField.getValue());
            }
        });
        widthPanel.add(widthField);


        /**Height Panel*/
        JPanel heightPanel = new JPanel();
        FlowLayout layout2 = new FlowLayout();
        heightPanel.setLayout(layout2);

        JLabel height = new JLabel("Height");
        heightPanel.add(height);

        heightField = new JSpinner(new SpinnerNumberModel(Constants.MAP_HEIGHT,20,300,5));
        heightField.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                heightValue = new Integer((int)heightField.getValue());
            }
        });
        heightPanel.add(heightField);


        /**Ground Panel*/
        JPanel groundPanel = new JPanel();
        FlowLayout layout3 = new FlowLayout();
        groundPanel.setLayout(layout3);

        JLabel ground = new JLabel("Ground %");
        groundPanel.add(ground);

        groundField = new JSpinner(new SpinnerNumberModel(Constants.GROUND_PERCENT * 100,10,100,5));
        groundField.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                groundValue = new Double((Double)groundField.getValue() / 100.0d);
            }
        });
        groundPanel.add(groundField);
        
        //Player field
        JPanel actionPanel = new JPanel();
        FlowLayout layout5 = new FlowLayout();
        heightPanel.setLayout(layout5);
        
        for(Player player:game.getPlayerList()){
        	JLabel pLabel = new JLabel(player.getName());
            actionPanel.add(pLabel);
            JLabel aLabel = new JLabel("Action Points");
            actionPanel.add(aLabel);
            JTextField actionPoints = new JTextField(player.getActionPoint().toString());
            actionPoints.setEditable(false);
            actionPanel.add(actionPoints);    
            JLabel eLabel = new JLabel("Energy");
            actionPanel.add(eLabel);
            JTextField energy = new JTextField(player.getEnergy().toString());
            energy.setEditable(false);
            actionPanel.add(energy);
        }
        
        
        //JTextField energy = new JTextField("Energy");
        //groundPanel.add(energy);


        /**Create Button*/
        JPanel buttonPanel = new JPanel();
        FlowLayout layout4 = new FlowLayout();
        buttonPanel.setLayout(layout4);

        JButton createButton = new JButton("Create Map");
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Constants.update(mapPanel.getWidth(), mapPanel.getHeight(), widthValue.intValue(), heightValue.intValue(), groundValue.doubleValue());
                mapPanel.regenerate();
            }
        });
        buttonPanel.add(createButton);
        
        //Create and add a Menu
        menubar = new JMenuBar();
        this.setJMenuBar(menubar);
        
        //Add stuff to the menu bar
        JMenu menuOption = new JMenu("Menu");
        JMenuItem endTurn = new JMenuItem("End Turn");
        menubar.add(menuOption);
        menubar.add(endTurn);
        

        //Add items on the menu
        JMenuItem statistics = new JMenuItem("Army summary");
        menuOption.add(statistics);
        
        
        //Add behavior to statistics
        statistics.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SummaryDialog dialog = new SummaryDialog(new JFrame(), "Statistics", game.getCurrentPlayer().summary());
			}
		});
        
        
        //Add behavior to endTurn
        endTurn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("clicked end turn!");
				game.turnEnd();
				game.getWorld().repaint();
			}
		});
        

        /**Add everything to the leftToolPanel*/
        leftToolbarPanel.add(widthPanel);
        leftToolbarPanel.add(heightPanel);
        leftToolbarPanel.add(groundPanel);
        leftToolbarPanel.add(buttonPanel);
        leftToolbarPanel.add(actionPanel);


        /**Add everything to ContentPane*/
        contentPane.add(leftToolbarPanel, BorderLayout.SOUTH);
        contentPane.add(mapPanel, BorderLayout.CENTER);


        /**Display the window*/
        pack();
        insets = getInsets();
        setSize(Constants.WINDOW_W, Constants.WINDOW_H + insets.top + buttonPanel.getHeight());
    }

	@Override
	public void componentResized(ComponentEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("Component resized at " + this.getWidth() + " " + this.getHeight());
		Constants.update(mapPanel.getWidth(), mapPanel.getHeight() - insets.top, Constants.MAP_WIDTH, Constants.MAP_HEIGHT, Constants.GROUND_PERCENT);
		mapPanel.scale();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	 public static void main(String[] args) {

	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                App example = new App();
	                example.setVisible(true);
	            }
	        });
	    }


}

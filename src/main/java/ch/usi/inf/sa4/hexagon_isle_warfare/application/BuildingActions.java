package ch.usi.inf.sa4.hexagon_isle_warfare.application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Building;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.GameStateMachine;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;

public class BuildingActions extends JDialog implements ActionListener {
	
	public BuildingActions(JFrame parent, String title, final Vector2D position, final GameStateMachine gsm, final Building building){
		super(parent, title, true);
        if (parent != null) {
            Dimension parentSize = parent.getSize();
            Point p = parent.getLocation();
            /**Position where the pop up appears*/
            setLocation((int)position.getX(), (int)position.getY());
        }

        JPanel actionPane = new JPanel();
        //JPanel internalMessagePane = new JPanel();

        //actionPane.setLayout(new BorderLayout());
        actionPane.setPreferredSize(new Dimension(200, 100));   //CHANGE DIMENSION TO THE MESSAGE
        
        
        //Add actions
        final List<JButton> actionsButton = new ArrayList<>();
        
        for (String action : building.retriveActions()) {
        	actionsButton.add(new JButton(action));
        	actionPane.add(actionsButton.get(actionsButton.size() - 1));
        }
        
        
        
        
        //actionPane.add(text, BorderLayout.CENTER);  //JTextArea is for editable text. not good in this example
        getContentPane().add(actionPane);
        JPanel buttonPane = new JPanel();
        JButton button = new JButton("OK");
        buttonPane.add(button);
        button.addActionListener(this);
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		setVisible(false);
        dispose();
	}

}

package ch.usi.inf.sa4.hexagon_isle_warfare.utilities;

public class ArrayCoordinate {
	private final int row;
	private final int column;
	
	/**
	 * Default class constructor
	 * @param row The row in the array
	 * @param col The column in the array
	 */
	public ArrayCoordinate(final int row, final int col) {
		this.row = row;
		this.column = col;
	}
	
	/**
	 * Getter for the row attribute
	 * @return The row in the coordinates
	 */
	public int getRow() {
		return row;
	}
	
	/**
	 * Getter for the column attribute
	 * @return The column in the coordinates
	 */
	public int getColumn() {
		return column;
	}
	
	
}

package ch.usi.inf.sa4.hexagon_isle_warfare.action;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Entity;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public class AttackAction extends Action {

	private final Unit actor;
	private Entity attacked;
	
	public AttackAction(final Player owner, final Unit actor, final Hexagon target) {
		super(owner, Constants.ATTACKACTIONLENGTH, Constants.ATTACKACTIONPOINTCOST, target);
		this.actor = actor;
	}

	public Unit getActor() {
		return actor;
	}

	public Entity getAttacked() {
		return attacked;
	}

	public void setAttacked(Entity attacked) {
		this.attacked = attacked;
	}

}

package ch.usi.inf.sa4.hexagon_isle_warfare.game;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.hexagon_isle_warfare.action.Action;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Building;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.HQ;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;
import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;


public class Player {

	private Integer energy;
	private Integer actionPoint;
	private final String name;
	private final List<Unit> unitList;
	private final List<Building> buildingList;
	private final List<Action> actionList;
	private final ArmySummary summary;
	
	public Player(final String name, final Integer actionPoint, final Integer energy){
		this.name = name;
		this.actionPoint = actionPoint;
		this.energy = energy;
		this.unitList = new ArrayList<Unit>();
		this.buildingList = new ArrayList<Building>();
		this.actionList = new ArrayList<Action>();
		summary = new ArmySummary(this);
	}
	
	public Integer getEnergy() {
		return this.energy;
	}
	public void setEnergy(final Integer energy) {
		this.energy = energy;
	}

	public Integer getActionPoint() {
		return this.actionPoint;
	}

	public void setActionPoint(final Integer actionPoint) {
		this.actionPoint = actionPoint;
	}

	public String getName() {
		return this.name;
	}

	public List<Unit> getUnitList() {
		return this.unitList;
	}


	public List<Building> getBuildingList() {
		return this.buildingList;
	}

	public List<Action> getActionList() {
		return this.actionList;
	}
	
	public void clearActionList(){
		this.actionList.clear();
	}
	
	/**
	 * Clear all the buildings and units of the player
	 */
	public void clear() {
		this.buildingList.clear();
		this.unitList.clear();
	}
	
	/**
	 * Creates the string for the army summary of the player
	 * @return A string representing the summary
	 */
	public String summary() {
		return summary.toString();
	}
	
	/**
	 * Check if the click is on the HQ
	 * @param click Position of the click
	 * @return True if the click is on the HQ, false otherwise
	 */
	public boolean HQClick(final Vector2D click) {
		for (Building building : buildingList) {
			try {
			if (building.getName().equals("HQ") && building.getCell().checkPointInside(click)) {
				System.out.println("HQ click");
				return true;
			}
			} catch (InvalidInputException ex) {
				System.out.println(ex.getMessage());
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 * @return
	 */
	public Building getPlayerHQ() {
		for (Building building : buildingList) {
			if (building.getName().equals("HQ")) {
				return building;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param mouseClick
	 */
	public Unit checkUnitClick(final Vector2D mouseClick) {
		for (Unit unit : unitList) {
			try {
				if (unit.getCell().checkPointInside(mouseClick)) {
					return unit;
				}
			} catch (InvalidInputException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}


	
}

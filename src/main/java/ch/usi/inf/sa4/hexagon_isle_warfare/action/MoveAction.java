package ch.usi.inf.sa4.hexagon_isle_warfare.action;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Player;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public class MoveAction extends Action{

	private final Unit actor;
	
	public MoveAction(final Player owner, final Unit actor, final Hexagon target) {
		super(owner, Constants.MOVEACTIONLENGTH, Constants.MOVEACTIONPOINTCOST, target);
		this.actor = actor;
	}

	public Unit getActor() {
		return actor;
	}

}

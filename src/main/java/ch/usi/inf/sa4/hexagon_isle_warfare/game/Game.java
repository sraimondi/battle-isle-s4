package ch.usi.inf.sa4.hexagon_isle_warfare.game;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import ch.usi.inf.sa4.hexagon_isle_warfare.application.HQActions;
import ch.usi.inf.sa4.hexagon_isle_warfare.application.MineActions;
import ch.usi.inf.sa4.hexagon_isle_warfare.application.UnitActions;
import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Building;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.HQ;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Mine;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.Unit;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.GameStateMachine;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.Transition;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.states.State;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.world.World;

public class Game {
	
	private final List<Player> playerList;
	private int currentPlayer;
	private World world;
	private Integer turnNumber = 1;
	private Integer turnEndedPlayers = 0;

	private final ActionVerifier actionVerifier;
	private final ActionResolver actionResolver;
	
	private final GameStateMachine gameFSM;
	
	public Game() {
		this.playerList = new ArrayList<>();
		this.world = new World(this.playerList);
		this.actionVerifier = new ActionVerifier(world.getMap(), getPlayerList());
		this.actionResolver = new ActionResolver(this.getPlayerList());
		
		this.gameFSM = new GameStateMachine(this);
		
		
		//Hard coded init players
		this.playerList.add(new Player("Jack", 10, 100));
		//this.playerList.add(new Player("Jon", 10, 100));
	
		currentPlayer = 0;
		
		spawnHQ();
	}
	
	
	public void notifyEndturn() {
		turnEndedPlayers += 1;
		if(turnEndedPlayers == getPlayerList().size())
			turnEnd();
	}
	
	public void turnEnd(){
		actionResolver.addPlayerActions();
		actionResolver.resolveActions();
		actionResolver.emptyLists();
		for(Player player : getPlayerList()){
			player.setActionPoint(Constants.BASEACTIONPOINTGAIN);
			player.setEnergy(player.getEnergy() + Constants.BASEENERGYGAIN);
			for(Building building : player.getBuildingList()){
				if(building instanceof Mine){
					player.setEnergy(player.getEnergy() + Constants.MINEPRODUCTIONENERGY);
				}
			}
			
		}
		//if (currentPlayer == playerList.size() - 1)
		//	augmentTurnNumber();
		//else 
		//	currentPlayer++;
	}	
	
	public void spawnHQ() {
		this.world.spawnHQ();
	}
	
	public World getWorld() {
		return this.world;
	}
	
	public void handleClick(final Vector2D mouseClick) {
		if (gameFSM.currentState() == State.WAITING_CLICK) {
			if (playerList.get(currentPlayer).HQClick(mouseClick)) {
				gameFSM.performEvent(Transition.HQ_CLICK);
				popUpHQAction(mouseClick);
			} else if (world.energyCellClick(mouseClick) != null) {
				gameFSM.performEvent(Transition.MINE_CLICK);
				popUpMineAction(mouseClick, world.energyCellClick(mouseClick));
			} else if (getCurrentPlayer().checkUnitClick(mouseClick) != null) {
				gameFSM.performEvent(Transition.UNIT_CLICK);
				popUpUnitAction(mouseClick, getCurrentPlayer().checkUnitClick(mouseClick));
			}
		} else if (gameFSM.currentState() == State.DISPLAY_HQ_ACTION) {

		} else if (gameFSM.currentState() == State.END) {

		}
	}


	public ActionVerifier getActionVerifier() {
		return this.actionVerifier;
	}


	public ActionResolver getActionResolver() {
		return this.actionResolver;
	}


	public Integer getTurnNumber() {
		return this.turnNumber;
	}


	public void augmentTurnNumber() {
		this.turnNumber++;
	}


	public List<Player> getPlayerList() {
		return playerList;
	}
	
	/**
	 * Getter for the current player of the turn
	 * @return The current player
	 */
	public Player getCurrentPlayer() {
		return playerList.get(currentPlayer);
	}
	
	/**
	 * Show hq actions
	 */
	public void popUpHQAction(final Vector2D position) {
		HQActions dialog = new HQActions(new JFrame(), "Actions", position, gameFSM, this, (HQ)getCurrentPlayer().getPlayerHQ());
	}
	
	public void popUpMineAction(final Vector2D position, final Hexagon cell) {	
		MineActions dialog = new MineActions(new JFrame(), "Actions", position, gameFSM, this, cell, getCurrentPlayer());
	}
	
	public void popUpUnitAction(final Vector2D position, final Unit unit) {
		UnitActions dialog = new UnitActions(new JFrame(), "Actions", position, gameFSM, this, unit);
	}
	

}

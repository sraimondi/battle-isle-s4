package ch.usi.inf.sa4.hexagon_isle_warfare.map;

/**
 * Different kind of super terrains.
 */
public enum SuperGroundTypes {
	WATER,
	GROUND
}

package ch.usi.inf.sa4.hexagon_isle_warfare.application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import ch.usi.inf.sa4.hexagon_isle_warfare.action.SpawnAction;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.ConcreteTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.entities.HQ;
import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.Game;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.GameStateMachine;
import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.Transition;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.GroundTypes;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.cell.Hexagon;

public class HQActions extends JDialog {

		public HQActions(JFrame parent, String title, final Vector2D position, final GameStateMachine gsm, final Game game, final HQ hq){
			super(parent, title, true);
	        if (parent != null) {
	            Dimension parentSize = parent.getSize();
	            Point p = parent.getLocation();
	            /**Position where the pop up appears*/
	            setLocation((int)position.getX(), (int)position.getY());
	        }

	        JPanel actionPane = new JPanel();
	        //JPanel internalMessagePane = new JPanel();

	        //actionPane.setLayout(new BorderLayout());
	        actionPane.setPreferredSize(new Dimension(200, 100));   //CHANGE DIMENSION TO THE MESSAGE
	        
	        
	        //Add actions

	        JButton spawn = new JButton("Spawn unit");
	        actionPane.add(spawn);
	        
	        spawn.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					gsm.performEvent(Transition.ACTION_CLICK);
					//Find spawn cell
					try {
						final HashMap<Integer, Hexagon> neighbours = game.getWorld().getBoard().getNeighbours(hq.getCell());
						Hexagon targetCell = null;
						for (Integer key : neighbours.keySet()) {
								targetCell = neighbours.get(key);
								final SpawnAction spawnAction = new SpawnAction(hq.getOwner(), hq, targetCell, ConcreteTypes.TEST_UNIT, 1);
								if (game.getActionVerifier().validateAction(spawnAction)) {
									hq.getOwner().getActionList().add(spawnAction);
									gsm.performEvent(Transition.ENDED_ACTION);
									setVisible(false);
							        dispose();
									return;
								}

						}
						gsm.performEvent(Transition.FAILED_ACTION);
						
						
						
					} catch (InvalidInputException ex) {
						System.out.println(ex.getMessage());
					}
					
					setVisible(false);
			        dispose();
				}
			});
	        	
	        
	        
	        
	        
	        //actionPane.add(text, BorderLayout.CENTER);  //JTextArea is for editable text. not good in this example
	        getContentPane().add(actionPane);
	        JPanel buttonPane = new JPanel();
	        JButton button = new JButton("OK");
	        buttonPane.add(button);
	        button.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					gsm.performEvent(Transition.EXIT_POPUP);
					setVisible(false);
			        dispose();
				}
			});
	        getContentPane().add(buttonPane, BorderLayout.SOUTH);
	        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	        pack();
	        setVisible(true);
			
		}
		

}

package ch.usi.inf.sa4.hexagon_isle_warfare.application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class SummaryDialog extends JDialog implements ActionListener {
	
	public SummaryDialog(JFrame parent, String title, String message){
		super(parent, title, true);
        if (parent != null) {
            Dimension parentSize = parent.getSize();
            Point p = parent.getLocation();
            /**Position where the pop up appears*/
            setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
        }

        JPanel messagePane = new JPanel();
        //JPanel internalMessagePane = new JPanel();

        messagePane.setLayout(new BorderLayout());
        messagePane.setPreferredSize(new Dimension(200, 100));   //CHANGE DIMENSION TO THE MESSAGE

        JTextArea text = new JTextArea(message);
        text.setEditable(false);

        messagePane.add(text, BorderLayout.CENTER);  //JTextArea is for editable text. not good in this example
        getContentPane().add(messagePane);
        JPanel buttonPane = new JPanel();
        JButton button = new JButton("OK");
        buttonPane.add(button);
        button.addActionListener(this);
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		setVisible(false);
        dispose();
	}

}

package ch.usi.inf.sa4.hexagon_isle_warfare.graphic;

import java.awt.*;

/**
 * @author Simone Raimondi
 * Created on 27/02/14.
 * This interface specify that the object which implements it is drawable
 */
public interface Drawable {
    /**
     * Abstract method to draw on a Graphics2D object
     * @param g The Graphics2D where you want to draw your class
     */
    public abstract void draw(Graphics2D g);

    /**
     * Abstract method to set the color of the object
     * @param newColor The new color of the object
     */
    public abstract void setColors(final Color... newColor);

}
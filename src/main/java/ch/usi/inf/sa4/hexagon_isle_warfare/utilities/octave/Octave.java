package ch.usi.inf.sa4.hexagon_isle_warfare.utilities.octave;


/**
 * @author Simone Raimondi & Filippo Ferrario
 * Created on 10/03/14.
 */
public class Octave {

    private final double noise[][];

    public Octave(final double[][] initNoise, final int k) {
        noise = generateSmoothNoise(initNoise, k);
    }

    private double[][] generateSmoothNoise(final double[][] initNoise, final int octave) {

        final int height = initNoise.length;
        final int width = initNoise[0].length;

        double smoothedNoise[][] = new double[height][width];

        int period = (int)Math.pow(2, octave);
        double frequency = 1.0 / period;

        for (int row = 0; row < height; row++) {
            int sample_i0 = (row / period) * period;
            int sample_i1 = (sample_i0 + period) % height;
            double vertical_blend = (row - sample_i0) * frequency;

            for (int col = 0; col < width; col++) {
                int sample_j0 = (col / period) * period;
                int sample_j1 = (sample_j0 + period) % width;
                double horizontal_blend = (col - sample_j0) * frequency;

                double topCorners = interpolate(initNoise[sample_i0][sample_j0], initNoise[sample_i0][sample_j1], horizontal_blend);

                double bottomCorners = interpolate(initNoise[sample_i1][sample_j0], initNoise[sample_i1][sample_j1], horizontal_blend);

                smoothedNoise[row][col] = interpolate(topCorners, bottomCorners, vertical_blend);
            }
        }

        return smoothedNoise;
    }


    private double interpolate(final double value_0, final double value_1, final double alpha) {
        return value_0 * (1 - alpha) + value_1 * alpha;
    }

    public double[][] getNoise() {
        return noise;
    }

}

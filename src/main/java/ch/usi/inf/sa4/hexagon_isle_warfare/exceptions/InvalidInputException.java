package ch.usi.inf.sa4.hexagon_isle_warfare.exceptions;

/**
 * @author Simone Raimondi
 * Created on 27/02/14.
 */
public class InvalidInputException extends Exception {

    public InvalidInputException(String message) {
        super(message);
    }
}
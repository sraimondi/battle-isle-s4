package ch.usi.inf.sa4.hexagon_isle_warfare.map;

import ch.usi.inf.sa4.hexagon_isle_warfare.constants.Constants;
import ch.usi.inf.sa4.hexagon_isle_warfare.event_listener.EventListener;
import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;
import ch.usi.inf.sa4.hexagon_isle_warfare.graphic.Drawable;
import ch.usi.inf.sa4.hexagon_isle_warfare.map.strategy.BoardStrategy;
import ch.usi.inf.sa4.hexagon_isle_warfare.utilities.ArrayCoordinate;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * @author Simone Raimondi
 * Created on 27/02/14.
 */
public class Map<T extends Drawable & EventListener> {
	private final int width;
	private final int height;
	private final List<T> cells;
    private BoardStrategy<T> boardType;

    /**
     * Constructor for the abstract Board class
     * @param w width of the map
     * @param h height of the map
     * @throws java.lang.IndexOutOfBoundsException if the height of the map or the width is less than zero, an
     */
    @SuppressWarnings({})
    public <E extends BoardStrategy<T>> Map(Class<T> classType, final int w, final int h, final BoardStrategy<T> strategy) throws InvalidInputException {
        if (w <= 0 || h <= 0) throw new InvalidInputException("Width or height is less than zero");
        else {
        	width = w;
        	height = h;
            cells = new ArrayList<>();
        }
        for (int index = 0; index < height * width; index++) {
        	cells.add(null);
        }
        if (strategy == null) throw new InvalidInputException("Strategy is null");
        else boardType = strategy;

    }
    

    /**
     *
     * @param row the row of the cell you want
     * @param col the column of the cell you want
     * @return a reference to the cell you want
     */
    public T getCellAt(final int row, final int col) throws IndexOutOfBoundsException {
        if (row < 0 || row >= height || col < 0 || col >= width) throw new IndexOutOfBoundsException();
        
        
        else return cells.get(row * width + col);
    }

    /**
     *
     * @param row The row of the cell you want to set
     * @param col The column of the cell you want to set
     * @param newCell The new value for the cell
     * @param <E> The type of newCell must be a subtype of the given one for the cells
     */
    public <E extends T> void setCell(final int row, final int col, final E newCell) {
        cells.set(row * width + col, newCell);
    }
    
    /**
     * Getter for the cells array
     * @return Reference to the cells array
     */
    public List<T> getCells() {
    	return cells;
    }
    
    /**
     * Getter for the width of the matrix
     * @return Width of the map
     */
    public int getWidth() {
    	return width;
    }
    
    /**
     * Getter for the height of the matrix
     * @return Height of the map
     */
    public int getHeight() {
    	return height;
    }

    /**
     * This method is responsible for drawing all the board
     * @param g The Graphic2D where you draw
     */
    public void draw(Graphics2D g) {
    	for (int index = 0; index < width * height; index++)
            cells.get(index).draw(g);
    }

    /**
     * Abstract method to handle a mouse click on the board
     * @param mouseClick The position of the click
     */
    public void handleMouseClick(final Vector2D mouseClick) {
    	
    }
    

    /**
     * Any operation that you need to do when refreshing the board (Colors, compute position, ecc)
     */
    public void refresh() {
    	for (int index = 0; index < width * height; index++) {
    		
    	}

    }
    
    public HashMap<Integer, T> getNeighbours(Integer row, Integer col) {
    	return boardType.getNeighbours(this, row, col);
    }
    
    public HashMap<Integer, T> getNeighbours(final T cell) throws InvalidInputException {
    	return boardType.getNeighbour(this, cell);
    }
}
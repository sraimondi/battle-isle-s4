package ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.states;

import ch.usi.inf.sa4.hexagon_isle_warfare.game.state_machine.Transition;

/**
 * 
 * @author Simone Raimondi
 * State enum to represent the possibles states that the game can be
 */
public enum State {
	WAITING_CLICK {
		@Override
        public State nextState(final Transition transition) {
            if (transition == Transition.HQ_CLICK) {
                return DISPLAY_HQ_ACTION;
            } else if (transition == Transition.MINE_CLICK) {
            	return DISPLAY_MINE_ACTION;
            }
            else {
                return WAITING_CLICK;
            }
        }
        
	},
	DISPLAY_MINE_ACTION {
		@Override
        public State nextState(final Transition transition) {
			if (transition == Transition.ACTION_CLICK) {
                return END;
            } else if (transition == Transition.EXIT_POPUP) {
            	return WAITING_CLICK;
            } else if (transition == Transition.FAILED_ACTION) {
            	return WAITING_CLICK;
            }
            else {
                return END;
            }
		}
	},
	DISPLAY_HQ_ACTION {
		@Override
        public State nextState(final Transition transition) {
            if (transition == Transition.ACTION_CLICK) {
                return END;
            } else if (transition == Transition.EXIT_POPUP) {
            	return WAITING_CLICK;
            } else if (transition == Transition.FAILED_ACTION) {
            	return WAITING_CLICK;
            }
            else {
                return END;
            }
        }

	},
	DISPLAY_UNIT_ACTION {
		@Override
        public State nextState(final Transition transition) {
            if (transition == Transition.ACTION_CLICK) {
                return SECOND_CLICK_WAIT;
            } else if (transition == Transition.EXIT_POPUP) {
            	return WAITING_CLICK;
            } else if (transition == Transition.FAILED_ACTION) {
            	return WAITING_CLICK;
            }
            else {
                return END;
            }
        }

	},
	SECOND_CLICK_WAIT {
		@Override
        public State nextState(final Transition transition) {
            if (transition == Transition.TARGET_CLICK) {
                return SECOND_CLICK_WAIT;
            } else if (transition == Transition.EXIT_POPUP) {
            	return WAITING_CLICK;
            } else if (transition == Transition.FAILED_ACTION) {
            	return WAITING_CLICK;
            }
            else {
                return END;
            }
        }

	},
	END {
		@Override
		public State nextState(Transition event) {
			if (event == Transition.ENDED_ACTION) {
				return WAITING_CLICK;
			}  else if (event == Transition.FAILED_ACTION) {
				return WAITING_CLICK;
			} else {
				return END;
			}
		}

		
		
	};
	/**
	 * Return the next state in the state machine
	 * @param event Event happened given to the state machine
	 * @return The next state
	 */
	public abstract State nextState(final Transition event);
	
}

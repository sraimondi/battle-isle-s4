package ch.usi.inf.sa4.hexagon_isle_warfare.event_listener;

import ch.usi.inf.sa4.hexagon_isle_warfare.exceptions.InvalidInputException;
import ch.usi.inf.sa4.hexagon_isle_warfare.geometry.Vector2D;

/**
 * @author Simone Raimondi
 * Created on 28/02/14.
 */
public interface EventListener {
    /**
     * Check if a point is inside the cell
     * @param point The actual point
     * @return True if the point is inside, false otherwise
     */
    public abstract boolean checkPointInside(final Vector2D point) throws InvalidInputException;
    /**
     * Handle the action of the listeners
     */
    public abstract void handleAction();
}

package ch.usi.inf.sa4.hexagon_isle_warfare.map;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SuperTypeRetriverTest {

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetTerrainSuperTypeGround() {
		assertEquals(SuperGroundTypes.GROUND, SuperTypeRetriver.getTerrainSuperType(GroundTypes.ENERGYCELL));
	}
	
	@Test
	public void testGetTerrainSuperTypeWater() {
		assertEquals(SuperGroundTypes.WATER, SuperTypeRetriver.getTerrainSuperType(GroundTypes.DEEPWATER));
	}

}
